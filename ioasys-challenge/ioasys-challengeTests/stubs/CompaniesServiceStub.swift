//
//  CompaniesServiceStub.swift
//  ioasys-challengeTests
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

@testable import ioasys_challenge
import Foundation

final class CompaniesServiceStub: CompaniesServiceProtocol {
    var mockCompaniesList: [Company]
    
    init (mockCompaniesList: [Company]) {
        self.mockCompaniesList = mockCompaniesList
    }

    func searchCompanies (searchQuery: String, completionHandler: @escaping (Result<[Company], Error>) -> Void) {
        completionHandler(.success(mockCompaniesList))
    }
    
    func fetchDetailsOfCompany (withId id: Int, completionHandler: @escaping (Result<Company, Error>) -> Void) {
        let company = mockCompaniesList.first { $0.id == id }
        if let company = company {
            completionHandler(.success(company))
            return
        }

        completionHandler(.failure(CompaniesServiceError.noDataReceived))
    }
}
