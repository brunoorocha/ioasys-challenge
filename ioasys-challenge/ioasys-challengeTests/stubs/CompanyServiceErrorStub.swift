//
//  CompanyServiceErrorStub.swift
//  ioasys-challengeTests
//
//  Created by Bruno Rocha on 05/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

@testable import ioasys_challenge
import Foundation

final class CompaniesServiceErrorStub: CompaniesServiceProtocol {
    func searchCompanies (searchQuery: String, completionHandler: @escaping (Result<[Company], Error>) -> Void) {
        completionHandler(.failure(CompaniesServiceError.noDataReceived))
    }

    func fetchDetailsOfCompany (withId id: Int, completionHandler: @escaping (Result<Company, Error>) -> Void) {
        completionHandler(.failure(CompaniesServiceError.noDataReceived))
    }
}
