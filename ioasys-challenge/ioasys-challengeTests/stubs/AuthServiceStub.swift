//
//  AuthServiceStub.swift
//  ioasys-challengeTests
//
//  Created by Bruno Rocha on 05/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

@testable import ioasys_challenge
import Foundation

final class AuthServiceStub: AuthServiceProtocol {
    func login (withCredentials credentials: UserCredentials, completionHandler: @escaping ((Result<Any?, Error>) -> Void)) {
        completionHandler(.success(nil))
    }
}
