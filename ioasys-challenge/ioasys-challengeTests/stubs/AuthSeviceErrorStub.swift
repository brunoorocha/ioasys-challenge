//
//  AuthSeviceErrorStub.swift
//  ioasys-challengeTests
//
//  Created by Bruno Rocha on 05/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

@testable import ioasys_challenge
import Foundation

final class AuthServiceErrorStub: AuthServiceProtocol {
    func login (withCredentials credentials: UserCredentials, completionHandler: @escaping ((Result<Any?, Error>) -> Void)) {
        completionHandler(.failure(AuthServiceError.expectedHeadersAreMissing))
    }
}

final class AuthServiceFailStub: AuthServiceProtocol {
    func login (withCredentials credentials: UserCredentials, completionHandler: @escaping ((Result<Any?, Error>) -> Void)) {
        completionHandler(.failure(AuthServiceError.credentialsAreInvalid))
    }
}
