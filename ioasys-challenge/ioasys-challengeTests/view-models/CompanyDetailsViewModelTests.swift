//
//  CompanyDetailsViewModelTests.swift
//  ioasys-challengeTests
//
//  Created by Bruno Rocha on 05/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

@testable import ioasys_challenge
import XCTest

class CompanyDetailsViewModelTests: XCTestCase {
    var mockCompany = Company(id: 1, name: "Empresa X", description: "", photo: nil)
    
    func test_shouldCallOnFetchCompanyDetailsStartWhenFetchCompanyDetailsWasCalled () {
        let service = CompaniesServiceStub(mockCompaniesList: [mockCompany])
        let viewModel = makeAHomeViewModel(withService: service)
        var onFetchCompanyDetailsStartWasCalled = false

        viewModel.onFetchCompanyDetailsStart = {
            onFetchCompanyDetailsStartWasCalled = true
        }

        viewModel.fetchDetailsOfCompany(withId: mockCompany.id)
        XCTAssertTrue(onFetchCompanyDetailsStartWasCalled)
    }

    func test_shouldCallOnFetchCompanyDetailsEndWhenFetchCompanyDetailsWasCalled () {
        let service = CompaniesServiceStub(mockCompaniesList: [mockCompany])
        let viewModel = makeAHomeViewModel(withService: service)
        var onFetchCompanyDetailsEndWasCalled = false

        viewModel.onFetchCompanyDetailsEnd = {
            onFetchCompanyDetailsEndWasCalled = true
        }

        viewModel.fetchDetailsOfCompany(withId: mockCompany.id)
        XCTAssertTrue(onFetchCompanyDetailsEndWasCalled)
    }

    func test_companyDetailsViewViewModelShouldNotBeNilWhenSuccess () {
        let service = CompaniesServiceStub(mockCompaniesList: [mockCompany])
        let viewModel = makeAHomeViewModel(withService: service)
        viewModel.fetchDetailsOfCompany(withId: mockCompany.id)

        XCTAssertNotNil(viewModel.companyDetailsViewViewModel)
        XCTAssertEqual(viewModel.companyDetailsViewViewModel?.name, mockCompany.name)
        XCTAssertEqual(viewModel.companyDetailsViewViewModel?.description, mockCompany.description)
    }

    func test_shouldCallOnFetchCompanyDetailsErrorWhenServiceReturnsAnError () {
        let service = CompaniesServiceErrorStub()
        let viewModel = makeAHomeViewModel(withService: service)
        var onFetchCompanyDetailsErrorWasCalled = false

        viewModel.onFetchCompanyDetailsError = { errorMessage in
            onFetchCompanyDetailsErrorWasCalled = true
        }

        viewModel.fetchDetailsOfCompany(withId: mockCompany.id)
        XCTAssertTrue(onFetchCompanyDetailsErrorWasCalled)
    }

    func makeAHomeViewModel (withService service: CompaniesServiceProtocol) -> CompanyDetailsViewModel {
        let viewModel = CompanyDetailsViewModel(service: service)
        return viewModel
    }
}
