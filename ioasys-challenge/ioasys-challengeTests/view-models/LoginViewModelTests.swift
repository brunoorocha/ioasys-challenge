//
//  LoginViewModelTests.swift
//  ioasys-challengeTests
//
//  Created by Bruno Rocha on 05/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

@testable import ioasys_challenge
import XCTest

class LoginViewModelTests: XCTestCase {
    func test_shoulCallOnLoginRequestStartWhenLoginMethodWasCalled () {
        let viewModel = makeLoginViewModel(service: AuthServiceStub())
        var onLoginRequestStartWasCalled = false

        viewModel.onLoginRequestStart = {
            onLoginRequestStartWasCalled = true
        }

        viewModel.login(email: "", password: "")
        XCTAssertTrue(onLoginRequestStartWasCalled)
    }
    
    func test_shoulCallOnLoginRequestEndWhenLoginMethodWasCalled () {
        let viewModel = makeLoginViewModel(service: AuthServiceStub())
        var onLoginRequestEndWasCalled = false

        viewModel.onLoginRequestEnd = {
            onLoginRequestEndWasCalled = true
        }

        viewModel.login(email: "", password: "")
        XCTAssertTrue(onLoginRequestEndWasCalled)
    }
    
    func test_shoulCallOnLoginRequestFailWhenServiceReturnsACredentialsAreInvalidError () {
        let viewModel = makeLoginViewModel(service: AuthServiceFailStub())
        var onLoginRequestFailWasCalled = false

        viewModel.onLoginRequestFail = {
            onLoginRequestFailWasCalled = true
        }

        viewModel.login(email: "", password: "")
        XCTAssertTrue(onLoginRequestFailWasCalled)
    }

    func test_shoulCallOnLoginRequestErrorWhenServiceReturnsAnError () {
        let viewModel = makeLoginViewModel(service: AuthServiceErrorStub())
        var onLoginRequestErrorWasCalled = false

        viewModel.onLoginRequestError = { errorMessage in
            onLoginRequestErrorWasCalled = true
        }

        viewModel.login(email: "", password: "")
        XCTAssertTrue(onLoginRequestErrorWasCalled)
    }
    
    func makeLoginViewModel (service: AuthServiceProtocol) -> LoginViewModel {
        let viewModel = LoginViewModel(authService: service)
        return viewModel
    }
}
