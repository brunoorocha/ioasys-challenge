//
//  HomeViewModelTests.swift
//  ioasys-challengeTests
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

@testable import ioasys_challenge
import XCTest

class HomeViewModelTests: XCTestCase {

    override func setUp() {}

    override func tearDown() {}
    
    func test_shouldCallOnSearchCompaniesStartWhenCallFetchCompaniesMethod () {
        let service = CompaniesServiceStub(mockCompaniesList: [])
        let viewModel = makeAHomeViewModel(withService: service)
        var onStartFetchingCompaniesWasCalled = false

        viewModel.onSearchCompaniesStart = {
            onStartFetchingCompaniesWasCalled = true
        }

        viewModel.searchCompanies()
        XCTAssertTrue(onStartFetchingCompaniesWasCalled)
    }

    func test_shouldCallOnSearchCompaniesEndWhenFetchCompaniesMethodWasCalled () {
        let service = CompaniesServiceStub(mockCompaniesList: [])
        let viewModel = makeAHomeViewModel(withService: service)
        var onEndFetchingCompaniesWasCalled = false

        viewModel.onSearchCompaniesEnd = {
            onEndFetchingCompaniesWasCalled = true
        }

        viewModel.searchCompanies()
        XCTAssertTrue(onEndFetchingCompaniesWasCalled)
    }

    func test_shouldReceiveCompaniesFromServiceAndCreateItsViewModels () {
        let mockCompanies = makeMockCompaniesList()
        let service = CompaniesServiceStub(mockCompaniesList: mockCompanies)
        let viewModel = makeAHomeViewModel(withService: service)
        viewModel.searchCompanies()

        XCTAssertEqual(viewModel.companiesViewModels.count, mockCompanies.count)
    }

    func test_shouldSetCompaniesCountWhenCompaniesAreFetched () {
        let mockCompanies = makeMockCompaniesList()
        let service = CompaniesServiceStub(mockCompaniesList: mockCompanies)
        let viewModel = makeAHomeViewModel(withService: service)
        viewModel.searchCompanies()

        XCTAssertEqual(viewModel.companiesCount, mockCompanies.count)
    }

    func test_shouldCallOnSearchCompaniesErrorWhenServiceReturnsAnError () {
        let service = CompaniesServiceErrorStub()
        let viewModel = makeAHomeViewModel(withService: service)
        var onSearchCompaniesErrorWasCalled = false

        viewModel.onSearchCompaniesError = { errorMessage in
            onSearchCompaniesErrorWasCalled = true
        }

        viewModel.searchCompanies()
        XCTAssertTrue(onSearchCompaniesErrorWasCalled)
    }

    func makeAHomeViewModel (withService service: CompaniesServiceProtocol) -> HomeViewModel {
        let viewModel = HomeViewModel(service: service)
        return viewModel
    }

    func makeMockCompaniesList () -> [Company] {
        let mockCompanies = [
            Company(id: 1, name: "Empresa X", description: "", photo: nil),
            Company(id: 1, name: "Empresa Y", description: "", photo: nil),
            Company(id: 1, name: "Empresa Z", description: "", photo: nil)
        ]

        return mockCompanies
    }
}
