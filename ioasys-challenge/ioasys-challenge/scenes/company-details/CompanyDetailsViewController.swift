//
//  CompanyDetailsViewController.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 04/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class CompanyDetailsViewController: CustomNavigationBarViewController {
    let companyDetailsView = CompanyDetailsView()
    let companyDetailsViewModel = CompanyDetailsViewModel()

    override func loadView() {
        view = companyDetailsView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bindToViewModels()
    }

    private func bindToViewModels () {
        companyDetailsViewModel.onFetchCompanyDetailsStart = { [weak self] in
            self?.fetchCompanyDetailsDidStart()
        }

        companyDetailsViewModel.onFetchCompanyDetailsEnd = { [weak self] in
            self?.fetchCompanyDetailsDidEnd()
        }

        companyDetailsViewModel.onFetchCompanyDetailsError = { [weak self] errorMessage in
            self?.fetchCompanyDetailsHasErrors(errorMessage: errorMessage)
        }
    }

    private func fetchCompanyDetailsDidStart () {
        companyDetailsView.showLoadingView()
    }

    private func fetchCompanyDetailsDidEnd () {
        companyDetailsView.hideLoadingView()
        guard let companyDetailsViewViewModel = companyDetailsViewModel.companyDetailsViewViewModel else { return }
        companyDetailsView.headerView.companyNameLabel.text = companyDetailsViewViewModel.name.uppercased()
        companyDetailsView.descriptionLabel.text = companyDetailsViewViewModel.description
        title = companyDetailsViewViewModel.name
    }

    private func fetchCompanyDetailsHasErrors (errorMessage: String) {
        presentAlertModal(title: "Houston, temos um problema", message: errorMessage, actionTitle: "Ok")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .darkContent
        }

        return .default
    }
}
