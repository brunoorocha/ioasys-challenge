//
//  CompanyDetailsViewModel.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 04/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

class CompanyDetailsViewModel {
    typealias EventClosure = (() -> Void)?
    var onFetchCompanyDetailsStart: EventClosure = nil
    var onFetchCompanyDetailsEnd: EventClosure = nil
    var onFetchCompanyDetailsError: ((String) -> Void)? = nil

    var companyDetailsViewViewModel: CompanyDetailsViewViewModel?

    let service: CompaniesServiceProtocol
    
    init(service: CompaniesServiceProtocol = ApiCompaniesService()) {
        self.service = service
    }

    func fetchDetailsOfCompany (withId id: Int) {
        onFetchCompanyDetailsStart?()
        service.fetchDetailsOfCompany(withId: id) { [weak self] result in
            switch (result) {
            case .success(let company):
                self?.companyDetailsViewViewModel = CompanyDetailsViewViewModel(name: company.name, description: company.description)
                self?.onFetchCompanyDetailsEnd?()

            case .failure(let error):
                self?.onFetchCompanyDetailsError?(error.localizedDescription)
            }
        }
    }
}
