//
//  CompanyDetailsView.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 04/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class CompanyDetailsView: UIView {
    let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let stackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .equalSpacing
        view.spacing = AppStyleGuide.Margins.medium.rawValue
        return view
    }()

    let contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let headerView = CompanyCardView()
    let descriptionLabel = ParagraphLabel()
    let loadingView = LoadingView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSubviews () {
        backgroundColor = AppStyleGuide.Colors.white.uiColor
        addSubview(scrollView)
        addSubview(loadingView)

        contentView.addSubview(descriptionLabel)
        scrollView.addSubview(stackView)

        stackView.addArrangedSubview(headerView)
        stackView.addArrangedSubview(contentView)
        
        let mediumMargin = AppStyleGuide.Margins.medium.rawValue
        let xlargeMargin = AppStyleGuide.Margins.xlarge.rawValue

        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor),

            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -xlargeMargin),

            loadingView.topAnchor.constraint(equalTo: topAnchor),
            loadingView.leadingAnchor.constraint(equalTo: leadingAnchor),
            loadingView.trailingAnchor.constraint(equalTo: trailingAnchor),
            loadingView.bottomAnchor.constraint(equalTo: bottomAnchor),

            descriptionLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: mediumMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: mediumMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -mediumMargin),
            descriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])

        hideLoadingView()
    }

    func showLoadingView () {
        loadingView.isHidden = false
    }

    func hideLoadingView () {
        loadingView.isHidden = true
    }
}
