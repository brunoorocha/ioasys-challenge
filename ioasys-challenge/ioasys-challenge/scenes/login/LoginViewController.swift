//
//  LoginViewController.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    let loginView = LoginView()
    let loginViewModel = LoginViewModel()

    override func loadView() {
        view = loginView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        configureDelegatesAndDatasources()
        configureTargets()
        bindToViewModel()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private func configureTargets () {
        loginView.submitButton.button.addTarget(self, action: #selector(self.didTapOnSubmitButton), for: .touchUpInside)
    }
    
    private func configureDelegatesAndDatasources () {
        loginView.formEmailField.textField.delegate = self
        loginView.formPasswordField.textField.delegate = self
    }

    private func bindToViewModel () {
        loginViewModel.onLoginRequestStart = { [weak self] in
            self?.didLoginRequestStarted()
        }

        loginViewModel.onLoginRequestEnd = { [weak self] in
            self?.loginRequestDidEnd()
        }

        loginViewModel.onLoginRequestSuccess = { [weak self] in
            self?.loginRequestHasSucceed()
        }

        loginViewModel.onLoginRequestFail = { [weak self] in
            self?.loginRequestDidFailed()
        }

        loginViewModel.onLoginRequestError = { [weak self] errorMessage in
            self?.loginRequestHasErrors(errorMessage: errorMessage)
        }
    }

    @objc func didTapOnSubmitButton () {
        guard let email = loginView.formEmailField.textField.text,
            let password = loginView.formPasswordField.textField.text else {
                return
        }

        loginViewModel.login(email: email, password: password)
    }

    private func didLoginRequestStarted () {
        self.loginView.showActivityIndicator()
    }

    private func loginRequestHasSucceed () {
        let homeViewController = HomeViewController()
        let navigationController = CustomNavigationController(rootViewController: homeViewController)
        guard let window = UIApplication.shared.windows.first else {
            presentAlertModal(title: "Houston, temos um problema", message: "Não foi possível entrar no app", actionTitle: "Ok")
            return
        }

        window.rootViewController = navigationController
    }

    private func loginRequestDidFailed () {
        loginView.hideActivityIndicator()
        loginView.formEmailField.hasErrors(errorMessage: "")
        loginView.formPasswordField.hasErrors(errorMessage: "Credenciais incorretas")
    }

    private func loginRequestDidEnd () {
        self.loginView.hideActivityIndicator()
    }

    private func loginRequestHasErrors (errorMessage: String) {
        self.loginView.hideActivityIndicator()
        presentAlertModal(title: "Houston, temos um problema", message: errorMessage, actionTitle: "Ok")
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let formField = [loginView.formEmailField, loginView.formPasswordField].first { $0.textField == textField }
        formField?.clearErrors()
    }
}
