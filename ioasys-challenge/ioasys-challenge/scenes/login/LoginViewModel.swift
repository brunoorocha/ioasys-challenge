//
//  LoginViewModel.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

class LoginViewModel {
    typealias EventClosure = (() -> Void)?
    var onLoginRequestStart: EventClosure = nil
    var onLoginRequestEnd: EventClosure = nil
    var onLoginRequestSuccess: EventClosure = nil
    var onLoginRequestFail: EventClosure = nil
    var onLoginRequestError: ((String) -> Void)? = nil
    
    let authService: AuthServiceProtocol
    
    init(authService: AuthServiceProtocol = ApiAuthService()) {
        self.authService = authService
    }

    func login (email: String, password: String) {
        onLoginRequestStart?()
        let credentials = UserCredentials(email: email, password: password)

        authService.login(withCredentials: credentials) { [weak self] result in
            switch (result) {
            case .success(_):
                self?.onLoginRequestSuccess?()
                self?.onLoginRequestEnd?()

            case .failure(let error):
                if let error = error as? AuthServiceError {
                    switch error {
                    case .credentialsAreInvalid:
                        self?.onLoginRequestFail?()
                    case .expectedHeadersAreMissing:
                        self?.onLoginRequestError?("")
                    case .others(let error):
                        self?.onLoginRequestError?(error.localizedDescription)
                    }
                }

                self?.onLoginRequestError?(error.localizedDescription)
            }
        }
    }
}
