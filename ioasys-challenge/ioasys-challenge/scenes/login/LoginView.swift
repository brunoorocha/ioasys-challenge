//
//  LoginView.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class LoginView: UIView {
    let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let stackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .equalSpacing
        view.spacing = AppStyleGuide.Margins.medium.rawValue
        return view
    }()
    
    var verticalSpacer: UIView {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: AppStyleGuide.Margins.medium.rawValue))
        return view
    }
    
    let headerView = GreetingHeaderView()
    let formEmailField = FormTextField()
    let formPasswordField = FormTextField()
    let submitButton = FormSubmitButton()
    
    private let fullscreenActivityIndicator = FullScreenActivityIndicator()

    private var scrollViewBottomConstraint: NSLayoutConstraint!

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = AppStyleGuide.Colors.white.uiColor
        configureSubviews()
        configureKeyboardEventObservers()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSubviews () {
        addSubview(scrollView)
        addSubview(fullscreenActivityIndicator)
        scrollView.addSubview(stackView)
        
        formEmailField.label.text = "Email"
        formEmailField.textField.keyboardType = .emailAddress
        formPasswordField.label.text = "Senha"
        formPasswordField.isSecureTextEntry = true
        fullscreenActivityIndicator.isHidden = true

        submitButton.title = "ENTRAR"

        stackView.addArrangedSubview(headerView)
        stackView.addArrangedSubview(verticalSpacer)
        
        stackView.addArrangedSubview(formEmailField)
        stackView.addArrangedSubview(formPasswordField)

        stackView.addArrangedSubview(verticalSpacer)
        stackView.addArrangedSubview(submitButton)

        scrollViewBottomConstraint = scrollView.bottomAnchor.constraint(equalTo: bottomAnchor)
        scrollView.keyboardDismissMode = .interactive

        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollViewBottomConstraint,
            
            fullscreenActivityIndicator.topAnchor.constraint(equalTo: topAnchor),
            fullscreenActivityIndicator.leadingAnchor.constraint(equalTo: leadingAnchor),
            fullscreenActivityIndicator.trailingAnchor.constraint(equalTo: trailingAnchor),
            fullscreenActivityIndicator.bottomAnchor.constraint(equalTo: bottomAnchor),

            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -AppStyleGuide.Margins.xlarge.rawValue),

            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
    }

    private func configureKeyboardEventObservers () {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    func showActivityIndicator () {
        fullscreenActivityIndicator.isHidden = false
        fullscreenActivityIndicator.show(animated: true)
    }

    func hideActivityIndicator () {
        fullscreenActivityIndicator.isHidden = true
        fullscreenActivityIndicator.hide(animated: true)
    }

    private func setScrollViewBottomConstraintConstant (to constant: CGFloat) {
        UIView.animate(
            withDuration: 0.25,
            delay: 0,
            options: [.curveEaseOut],
            animations: {
            self.scrollViewBottomConstraint.constant = constant
            self.layoutIfNeeded()
        })
    }

    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize: CGRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        setScrollViewBottomConstraintConstant(to: -keyboardSize.size.height)
        headerView.colapse(animated: true)
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        setScrollViewBottomConstraintConstant(to: 0)
        headerView.open(animated: true)
    }
}
