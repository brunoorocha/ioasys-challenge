//
//  HomeView.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class HomeView: UIView {

    let companiesTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        tableView.keyboardDismissMode = .interactive
        tableView.backgroundColor = AppStyleGuide.Colors.white.uiColor
        return tableView
    }()
    
    let searchHeaderView = SearchHeaderView()
    
    let tableSectionHeaderView = ResultsCountSectionHeaderView()
    let emptyView = NoResultsFoundView()
    let loadingView = LoadingView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = AppStyleGuide.Colors.white.uiColor
        configureSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureSubviews () {
        addSubview(searchHeaderView)
        addSubview(companiesTableView)
        addSubview(emptyView)
        addSubview(loadingView)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapOnEmptyView))
        emptyView.addGestureRecognizer(tapGestureRecognizer)
        emptyView.isUserInteractionEnabled = true

        NSLayoutConstraint.activate([
            searchHeaderView.topAnchor.constraint(equalTo: topAnchor),
            searchHeaderView.leadingAnchor.constraint(equalTo: leadingAnchor),
            searchHeaderView.trailingAnchor.constraint(equalTo: trailingAnchor),

            companiesTableView.topAnchor.constraint(equalTo: searchHeaderView.bottomAnchor),
            companiesTableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            companiesTableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            companiesTableView.bottomAnchor.constraint(equalTo: bottomAnchor),

            emptyView.topAnchor.constraint(equalTo: searchHeaderView.bottomAnchor),
            emptyView.leadingAnchor.constraint(equalTo: leadingAnchor),
            emptyView.trailingAnchor.constraint(equalTo: trailingAnchor),
            emptyView.bottomAnchor.constraint(equalTo: bottomAnchor),

            loadingView.topAnchor.constraint(equalTo: searchHeaderView.bottomAnchor),
            loadingView.leadingAnchor.constraint(equalTo: leadingAnchor),
            loadingView.trailingAnchor.constraint(equalTo: trailingAnchor),
            loadingView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])

        showInitialState()
    }

    func showInitialState () {
        emptyView.isHidden = true
        loadingView.isHidden = true
        companiesTableView.isHidden = true
    }

    func showCompaniesTableView () {
        emptyView.isHidden = true
        loadingView.isHidden = true
        companiesTableView.isHidden = false
    }

    func showLoadingView () {
        companiesTableView.isHidden = true
        emptyView.isHidden = true
        loadingView.isHidden = false
    }

    func showEmptyView () {
        companiesTableView.isHidden = true
        loadingView.isHidden = true
        emptyView.isHidden = false
    }
    
    @objc func didTapOnEmptyView () {
        searchHeaderView.searchField.resignFirstResponder()
        showInitialState()
    }
}
