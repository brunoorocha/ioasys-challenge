//
//  HomeViewModel.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

class HomeViewModel {
    var companiesViewModels: [CompanyCellViewModel] = []
    var companiesCount: Int {
        return companiesViewModels.count
    }

    typealias EventClosure = (() -> Void)?
    var onSearchCompaniesStart: EventClosure = nil
    var onSearchCompaniesEnd: EventClosure = nil
    var onSearchCompaniesError: ((String) -> Void)? = nil

    let service: CompaniesServiceProtocol
    
    init(service: CompaniesServiceProtocol = ApiCompaniesService()) {
        self.service = service
    }

    func searchCompanies (searchQuery: String = "") {
        onSearchCompaniesStart?()
        service.searchCompanies(searchQuery: searchQuery) { [weak self] result in
            switch (result) {
            case .success(let companies):
                self?.companiesViewModels = companies.map { CompanyCellViewModel(id: $0.id, name: $0.name) }
                self?.onSearchCompaniesEnd?()
            case .failure(let error):
                self?.onSearchCompaniesError?(error.localizedDescription)
            }
        }
    }

    func clearCompaniesViewModels () {
        companiesViewModels = []
        self.onSearchCompaniesEnd?()
    }
}
