//
//  HomeViewController.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    let homeView = HomeView()
    let homeViewModel = HomeViewModel()
    let companyTableViewCellIdentifier = "CompanyTableViewCellIdentifier"

    override func loadView() {
        view = homeView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureDelegatesAndDataSources()
        registerTableViewCells()
        bindToViewModel()
    }

    private func configureDelegatesAndDataSources () {
        homeView.companiesTableView.delegate = self
        homeView.companiesTableView.dataSource = self
        homeView.searchHeaderView.delegate = self
    }
    
    private func registerTableViewCells () {
        homeView.companiesTableView.register(CompanyTableViewCell.self, forCellReuseIdentifier: companyTableViewCellIdentifier)
    }

    private func bindToViewModel () {
        homeViewModel.onSearchCompaniesStart = { [weak self] in
            self?.searchCompaniesDidStart()
        }

        homeViewModel.onSearchCompaniesEnd = { [weak self] in
            self?.searchCompaniesDidEnd()
        }
        
        homeViewModel.onSearchCompaniesError = { [weak self] errorMessage in
            self?.searchCompaniesHasErrors(errorMessage: errorMessage)
        }
    }

    private func searchCompaniesDidStart () {
        self.homeView.showLoadingView()
    }

    private func searchCompaniesDidEnd () {
        self.homeView.tableSectionHeaderView.resultsCount = self.homeViewModel.companiesCount

        if (self.homeViewModel.companiesCount == 0) {
            self.homeView.showEmptyView()
            return
        }

        self.homeView.showCompaniesTableView()
        self.homeView.companiesTableView.reloadData()
    }

    private func searchCompaniesHasErrors (errorMessage: String) {
        presentAlertModal(title: "Houston, temos um problema", message: errorMessage, actionTitle: "Ok")
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    private func navigateToCompanyDetails (viewModelIndex: Int) {
        let companyDetailsViewController = CompanyDetailsViewController()
        let companyViewModel = homeViewModel.companiesViewModels[viewModelIndex]

        companyDetailsViewController.companyDetailsViewModel.fetchDetailsOfCompany(withId: companyViewModel.id)
        companyDetailsViewController.companyDetailsView.headerView.backgroundColor = backgroundColorForCell(at: viewModelIndex)
        navigationController?.pushViewController(companyDetailsViewController, animated: true)
    }
    
    private func backgroundColorForCell (at cellIndex: Int) -> UIColor {
        let colors = [AppStyleGuide.Colors.blue.uiColor, AppStyleGuide.Colors.solmon.uiColor, AppStyleGuide.Colors.green.uiColor]
        let colorIndex = cellIndex % colors.count
        return colors[colorIndex]
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return homeView.tableSectionHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 48
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeViewModel.companiesCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: companyTableViewCellIdentifier, for: indexPath) as? CompanyTableViewCell else {
            return UITableViewCell()
        }

        cell.companyCardView.companyNameLabel.text = homeViewModel.companiesViewModels[indexPath.row].name
        cell.companyCardView.backgroundColor = backgroundColorForCell(at: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigateToCompanyDetails(viewModelIndex: indexPath.row)
    }
}

extension HomeViewController: SearchHeaderViewDelegate {
    func searchQueryDidChange(query: String) {
        if (query.count < 3) {
            homeViewModel.clearCompaniesViewModels()
            return
        }

        homeViewModel.searchCompanies(searchQuery: query)
    }

    func onSearchFieldFocus() {
        homeView.searchHeaderView.colapse(animated: true)
    }

    func onSearchFieldBlur() {
        if (homeViewModel.companiesCount == 0) {
            homeView.searchHeaderView.open(animated: true)
        }
    }
}
