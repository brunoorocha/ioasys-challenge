//
//  CompanyCellViewModel.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 04/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

struct CompanyCellViewModel {
    let id: Int
    let name: String
}
