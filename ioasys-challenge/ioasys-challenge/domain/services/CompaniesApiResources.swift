//
//  CompaniesApiResources.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

enum CompaniesApiResources {
    case auth(email: String, password: String)
    case getAllCompanies(searchQuery: String)
    case getCompany(id: Int)
}

extension CompaniesApiResources: ApiResource {
    var apiUrl: String {
        return "https://empresas.ioasys.com.br/api/v1"
    }

    var endpoint: String {
        switch self {
        case .auth:
            return "/users/auth/sign_in"
        case .getAllCompanies(let searchQuery):
            return String(format: "/enterprises?name=%@", searchQuery)
        case .getCompany(let companyId):
            return String(format: "/enterprises/%d", companyId)
        }
    }

    var fullPathUrl: URL? {
        let path = apiUrl + endpoint
        return URL(string: path)
    }

    var httpMethod: HttpMethods {
        switch self {
        case .auth:
            return .post
        case .getAllCompanies:
            return .get
        case .getCompany:
            return .get
        }
    }

    var headers: [String : String] {
        let userDefaultsManager = UserDefaultsManager.shared
        var headers = ["Accept": "application/json"]

        guard let accessToken = userDefaultsManager.accessToken,
            let client = userDefaultsManager.client,
            let uid = userDefaultsManager.uid else {
                return headers
        }

        headers["access-token"] = accessToken
        headers["client"] = client
        headers["uid"] = uid
        return headers
    }
    
    var body: String? {
        switch self {
        case .auth(let email, let password):
            return String(format: "email=%@&password=%@", email, password)
        default:
            return nil
        }
    }
}
