//
//  CompaniesServiceProtocol.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

protocol CompaniesServiceProtocol {
    func searchCompanies (searchQuery: String, completionHandler: @escaping (Result<[Company], Error>) -> Void)
    func fetchDetailsOfCompany (withId id: Int, completionHandler: @escaping (Result<Company, Error>) -> Void)
}
