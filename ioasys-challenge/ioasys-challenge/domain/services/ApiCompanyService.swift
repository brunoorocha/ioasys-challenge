//
//  ApiCompanyService.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

enum CompaniesServiceError: Error {
    case onDecode
    case noDataReceived
    case other(error: Error)
}

final class ApiCompaniesService: CompaniesServiceProtocol {

    let networkService: NetworkServiceProtocol
    
    init(networkService: NetworkServiceProtocol = NetworkService()) {
        self.networkService = networkService
    }

    func searchCompanies (searchQuery: String, completionHandler: @escaping (Result<[Company], Error>) -> Void) {
        networkService.request(resource: CompaniesApiResources.getAllCompanies(searchQuery: searchQuery)) { result in
            switch (result) {
            case .success(let response):
                self.handleSearchCompaniesSuccess(requestResponse: response, completionHandler: completionHandler)
            case .failure(let error):
                completionHandler(.failure(CompaniesServiceError.other(error: error)))
            }
        }
    }
    
    func fetchDetailsOfCompany(withId id: Int, completionHandler: @escaping (Result<Company, Error>) -> Void) {
        networkService.request(resource: CompaniesApiResources.getCompany(id: id)) { result in
            switch (result) {
            case .success(let response):
                self.handleFetchDetailsOfCompanySuccess(requestResponse: response, completionHandler: completionHandler)
            case .failure(let error):
                completionHandler(.failure(CompaniesServiceError.other(error: error)))
            }
        }
    }

    private func handleSearchCompaniesSuccess (
        requestResponse: RequestResponse?,
        completionHandler: (Result<[Company], Error>) -> Void
    ) {
        guard let requestResponse = requestResponse, let data = requestResponse.data else {
            completionHandler(.success([]))
            return
        }

        do {
            let decodedResult = try JSONDecoder().decode(CompaniesApiResult.self, from: data)
            completionHandler(.success(decodedResult.enterprises))
            return
        }
        catch (let error) {
            debugPrint(error.localizedDescription)
            completionHandler(.failure(CompaniesServiceError.onDecode))
        }
    }
    
    private func handleFetchDetailsOfCompanySuccess (
        requestResponse: RequestResponse?,
        completionHandler: (Result<Company, Error>) -> Void
    ) {
        guard let requestResponse = requestResponse, let data = requestResponse.data else {
            completionHandler(.failure(CompaniesServiceError.noDataReceived))
            return
        }

        do {
            let decodedResult = try JSONDecoder().decode(CompanyApiResult.self, from: data)
            completionHandler(.success(decodedResult.enterprise))
            return
        }
        catch (let error) {
            debugPrint(error.localizedDescription)
            completionHandler(.failure(CompaniesServiceError.onDecode))
        }
    }
}
