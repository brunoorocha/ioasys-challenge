//
//  AuthServiceProtocol.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

protocol AuthServiceProtocol {
    func login (
        withCredentials credentials: UserCredentials,
        completionHandler: @escaping ((Result<Any?, Error>) -> Void)
    )
}
