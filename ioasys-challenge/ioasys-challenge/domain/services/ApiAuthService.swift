//
//  ApiAuthService.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

enum AuthServiceError: Error {
    case expectedHeadersAreMissing
    case credentialsAreInvalid
    case others(error: Error)
}

final class ApiAuthService: AuthServiceProtocol {
    let networkService: NetworkServiceProtocol

    init(networkService: NetworkServiceProtocol = NetworkService()) {
        self.networkService = networkService
    }

    func login(
        withCredentials credentials: UserCredentials,
        completionHandler: @escaping ((Result<Any?, Error>) -> Void)
    ) {
        let resource = CompaniesApiResources.auth(email: credentials.email, password: credentials.password)
        networkService.request(resource: resource) { result in
            switch (result) {
            case .success(let response):
                self.handleSuccessCase(requestResponse: response, completionHandler: completionHandler)
            case .failure(let error):
                self.handleFailureCase(error: error, completionHandler: completionHandler)
            }
        }
    }

    private func handleSuccessCase (
        requestResponse: RequestResponse?,
        completionHandler: ((Result<Any?, Error>) -> Void)
    ) {
        guard let requestResponse = requestResponse,
            let response = requestResponse.response as? HTTPURLResponse else {
            completionHandler(.failure(AuthServiceError.expectedHeadersAreMissing))
            return
        }

        let headers = response.allHeaderFields
        guard let accessToken = headers["access-token"] as? String,
            let client = headers["client"] as? String,
            let uid = headers["uid"] as? String else {
                completionHandler(.failure(AuthServiceError.expectedHeadersAreMissing))
                return
        }

        let userDefaultsManager = UserDefaultsManager.shared
        userDefaultsManager.setAccessToken(token: accessToken)
        userDefaultsManager.setClient(token: client)
        userDefaultsManager.setUID(token: uid)
        completionHandler(.success(nil))
    }
    
    private func handleFailureCase (
        error: NetworkServiceError,
        completionHandler: ((Result<Any?, Error>) -> Void)
    ) {
        switch error {
        case .unauthorized:
            completionHandler(.failure(AuthServiceError.credentialsAreInvalid))
        default:
            completionHandler(.failure(AuthServiceError.others(error: error)))
        }
    }
}
