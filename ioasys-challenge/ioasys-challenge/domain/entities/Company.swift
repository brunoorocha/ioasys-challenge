//
//  Company.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

struct Company {
    let id: Int
    let name: String
    let description: String
    let photo: String?
}

extension Company: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case name = "enterprise_name"
        case description
        case photo
    }
}

struct CompanyApiResult {
    let enterprise: Company
}

extension CompanyApiResult: Decodable {}


struct CompaniesApiResult {
    let enterprises: [Company]
}

extension CompaniesApiResult: Decodable {}
