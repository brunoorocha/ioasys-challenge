//
//  UserCredentials.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

struct UserCredentials {
    let email: String
    let password: String
}
