//
//  NetworkServiceError.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

enum NetworkServiceError: Error {
    case invalidUrl
    case noInternetConnection
    case connectionCancelled
    case resourceNotFount
    case unauthorized
    case serverError
    case unknown

    var localizedDescription: String {
        switch self {
        case .invalidUrl, .resourceNotFount:
            return "Desculpe, mas não foi possível encontrar nossos servidores."
        case .unauthorized:
            return "Desculpe, mas você não tem permissão para acessar esse recurso."
        case .serverError:
            return "Desculpe, mas houve algum problema com os nossos servidores."
        case .connectionCancelled, .noInternetConnection:
            return "Desculpe, mas não foi possível conectar aos nossos servidores, por favor verifique se o seu dispositivo possui acesso à internet."
        case .unknown:
            return "Desculpe, mas ocorreu um problema desconhecido."
        }
    }
}
