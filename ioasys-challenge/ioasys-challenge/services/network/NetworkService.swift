//
//  NetworkService.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

final class NetworkService: NetworkServiceProtocol {
    private var task: URLSessionTask!

    func request (resource: ApiResource, completionHandler: @escaping (Result<RequestResponse?, NetworkServiceError>) -> Void) {
        let urlSession = URLSession.shared

        guard let resourceUrl = resource.fullPathUrl else {
            completionHandler(.failure(.invalidUrl))
            return
        }

        var request = URLRequest(url: resourceUrl)
        request.httpMethod = resource.httpMethod.rawValue
        resource.headers.forEach ({ request.addValue($1, forHTTPHeaderField: $0) })

        if let bodyString = resource.body {
            if let bodyData = bodyString.data(using: String.Encoding.utf8) {
                request.httpBody = bodyData
                request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            }
        }

        self.task = urlSession.dataTask(with: request) { responseData, response, error in
            if let errorResponse = response as? HTTPURLResponse, (400..<600).contains(errorResponse.statusCode) {
                switch errorResponse.statusCode {
                case 401:
                    DispatchQueue.main.async {
                        completionHandler(.failure(.unauthorized))
                        return
                    }
                default:
                    DispatchQueue.main.async {
                        completionHandler(.failure(.serverError))
                        return
                    }
                }

                return
            }

            if let error = error {
                var networkServiceError: NetworkServiceError

                switch error._code {
                case NSURLErrorNotConnectedToInternet:
                    networkServiceError = .noInternetConnection
                case NSURLErrorCancelled:
                    networkServiceError = .connectionCancelled
                default:
                    networkServiceError = .unknown
                }

                DispatchQueue.main.async {
                    completionHandler(.failure(networkServiceError))
                    return
                }
            }

            let requestResponse = RequestResponse(data: responseData, response: response)
            DispatchQueue.main.async {
                completionHandler(.success(requestResponse))
                return
            }
        }

        self.task.resume()
    }
}
