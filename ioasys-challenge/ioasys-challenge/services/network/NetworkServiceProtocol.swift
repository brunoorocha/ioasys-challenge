//
//  NetworkServiceProtocol.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

protocol NetworkServiceProtocol {
    func request (resource: ApiResource, completionHandler: @escaping (Result<RequestResponse?, NetworkServiceError>) -> Void)
}
