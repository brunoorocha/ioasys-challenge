//
//  ApiResource.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

enum HttpMethods: String {
    case get = "GET"
    case post = "POST"
    case patch = "PATCH"
    case delete = "DELETE"
}

protocol ApiResource {
    var apiUrl: String { get }
    var endpoint: String { get }
    var fullPathUrl: URL? { get }
    var httpMethod: HttpMethods { get }
    var headers: [String: String] { get }
    var body: String? { get }
}
