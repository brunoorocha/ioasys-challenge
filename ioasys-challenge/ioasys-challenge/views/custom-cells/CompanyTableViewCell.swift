//
//  CompanyTableViewCell.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 04/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class CompanyTableViewCell: UITableViewCell {
    let companyCardView = CompanyCardView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSubviews () {
        selectionStyle = .none
        backgroundColor = AppStyleGuide.Colors.white.uiColor
        contentView.addSubview(companyCardView)

        let mediumMargin = AppStyleGuide.Margins.medium.rawValue
        companyCardView.layer.cornerRadius = 4

        NSLayoutConstraint.activate([
            companyCardView.topAnchor.constraint(equalTo: contentView.topAnchor),
            companyCardView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: mediumMargin),
            companyCardView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -mediumMargin),
            companyCardView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -mediumMargin),

            contentView.heightAnchor.constraint(equalTo: companyCardView.heightAnchor, constant: mediumMargin)
        ])
    }
}
