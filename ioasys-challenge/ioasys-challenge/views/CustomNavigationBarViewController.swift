//
//  CustomNavigationBarViewController.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 04/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class CustomNavigationBarViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        customize()
    }

    private func customize () {
        let customBackButton = CustomBackButton()
        customBackButton.addTarget(self, action: #selector(self.popNavigationToParent), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: customBackButton)

        navigationController?.navigationBar.tintColor = AppStyleGuide.Colors.pink.uiColor
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)

        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }

    @objc func popNavigationToParent () {
        navigationController?.popViewController(animated: true)
    }
}


extension CustomNavigationBarViewController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
