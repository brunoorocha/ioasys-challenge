//
//  CustomNavigationController.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 04/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        navigationControllerCustomization()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func navigationControllerCustomization () {
        let navbarTitleAttributes = [
            NSAttributedString.Key.font: AppStyleGuide.Typography.heading1.uiFont,
            NSAttributedString.Key.foregroundColor: AppStyleGuide.Colors.black.uiColor
        ]

        navigationBar.barTintColor = AppStyleGuide.Colors.white.uiColor
        navigationBar.titleTextAttributes = navbarTitleAttributes
        navigationBar.isTranslucent = true
        navigationBar.prefersLargeTitles = false
    }
}
