//
//  NoResultsFoundView.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class NoResultsFoundView: UIView {
    let label = ParagraphLabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureSubviews () {
        backgroundColor = AppStyleGuide.Colors.white.uiColor
        translatesAutoresizingMaskIntoConstraints = false

        label.text = "Nenhum resultado encontrado"
        label.textAlignment = .center
        label.textColor = AppStyleGuide.Colors.gray2.uiColor
        addSubview(label)

        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: centerXAnchor),
            label.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
}
