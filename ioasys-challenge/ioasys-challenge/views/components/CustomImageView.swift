//
//  CustomImageView.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//


import UIKit

class CustomImageView: UIImageView {
    init() {
        super.init(frame: .zero)
        defaultImageConfiguration()
        customize()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultImageConfiguration()
        customize()
    }

    override init(image: UIImage?) {
        super.init(image: image)
        defaultImageConfiguration()
        customize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func defaultImageConfiguration () {
        translatesAutoresizingMaskIntoConstraints = false
        clipsToBounds = true
        contentMode = .scaleAspectFill
    }

    func customize () {}
}
