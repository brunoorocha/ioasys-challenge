//
//  Button.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class Button: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultButtonConfiguration()
        customize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func defaultButtonConfiguration () {
        setTitleColor(AppStyleGuide.Colors.gray4.uiColor, for: .normal)
        titleLabel?.font = AppStyleGuide.Typography.button.uiFont
        translatesAutoresizingMaskIntoConstraints = false
    }

    func customize () {}
}
