//
//  Heading2Label.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class Heading2Label: BaseLabel {
    override func customize() {
        font = AppStyleGuide.Typography.heading2.uiFont
        textColor = AppStyleGuide.Colors.black.uiColor
    }
}
