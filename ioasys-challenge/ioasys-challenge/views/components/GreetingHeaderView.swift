//
//  GreetingHeaderView.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class GreetingHeaderView: UIView {

    var logoImageView = CustomImageView(image: AppStyleGuide.Images.ioasysLogo.uiImage)
    var backgroundImageView = CustomImageView(image: AppStyleGuide.Images.loginHeader.uiImage)
    var greetingLabel = Heading3Label()
    var heightConstraint: NSLayoutConstraint!
    var greetingLabelTopContraint: NSLayoutConstraint!
    
    let openHeight: CGFloat = 257
    let colapsedHeight: CGFloat = 128

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSubviews () {
        translatesAutoresizingMaskIntoConstraints = false

        addSubview(backgroundImageView)
        addSubview(logoImageView)
        addSubview(greetingLabel)

        let mediumMargin = AppStyleGuide.Margins.medium.rawValue
        greetingLabel.textColor = AppStyleGuide.Colors.white.uiColor
        greetingLabel.textAlignment = .center
        greetingLabel.text = "Seja bem vindo ao empresas!"
        logoImageView.contentMode = .scaleAspectFit

        heightConstraint = heightAnchor.constraint(equalToConstant: openHeight)
        greetingLabelTopContraint = greetingLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: mediumMargin)

        NSLayoutConstraint.activate([
            backgroundImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            backgroundImageView.bottomAnchor.constraint(equalTo: bottomAnchor),
            backgroundImageView.heightAnchor.constraint(equalToConstant: openHeight),

            logoImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            logoImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            logoImageView.widthAnchor.constraint(equalToConstant: 40),
            logoImageView.heightAnchor.constraint(equalToConstant: 40),

            greetingLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -mediumMargin),
            greetingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: mediumMargin),
            greetingLabelTopContraint,

            heightConstraint
        ])
    }

    func open (animated: Bool = false) {
        updateViewHeightConstraint(height: openHeight, animated: animated)
        animateGreetingLabel(opacity: 1, topConstraintConstant: AppStyleGuide.Margins.medium.rawValue, animated: true)
    }

    func colapse (animated: Bool = false) {
        updateViewHeightConstraint(height: colapsedHeight, animated: animated)
        animateGreetingLabel(opacity: 0, topConstraintConstant: 0, animated: true)
    }

    private func updateViewHeightConstraint (height: CGFloat, animated: Bool = false) {
        if animated {
            UIView.animate(
                withDuration: 0.25,
                delay: 0,
                options: [.curveEaseOut],
                animations: {
                    self.heightConstraint.constant = height
                    self.layoutIfNeeded()
                })

            return
        }

        heightConstraint.constant = height
    }
    
    private func animateGreetingLabel (opacity: Float, topConstraintConstant: CGFloat, animated: Bool = false) {
        if animated {
            UIView.animate(
                withDuration: 0.25,
                delay: 0,
                options: [.curveEaseOut],
                animations: {
                    self.greetingLabel.layer.opacity = opacity
                    self.greetingLabelTopContraint.constant = topConstraintConstant
                    self.layoutIfNeeded()
                })

            return
        }

        self.greetingLabel.layer.opacity = opacity
    }
}
