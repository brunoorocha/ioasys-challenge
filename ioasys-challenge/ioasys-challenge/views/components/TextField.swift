//
//  TextField.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class TextField: UITextField {

    var paddingInsets: UIEdgeInsets {
        UIEdgeInsets(top: 0, left: AppStyleGuide.Margins.medium.rawValue, bottom: 0, right: AppStyleGuide.Margins.medium.rawValue)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        customize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func customize() {
        translatesAutoresizingMaskIntoConstraints = false
        font = AppStyleGuide.Typography.textInput.uiFont
        textColor = AppStyleGuide.Colors.black.uiColor
        backgroundColor = AppStyleGuide.Colors.gray1.uiColor
        layer.borderColor = AppStyleGuide.Colors.gray1.uiColor.cgColor
        layer.borderWidth = 1
        layer.cornerRadius = 4
        clipsToBounds = true
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddingInsets)
    }

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddingInsets)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddingInsets)
    }
}
