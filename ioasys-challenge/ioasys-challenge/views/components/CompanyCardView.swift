//
//  CompanyCardView.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 04/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class CompanyCardView: UIView {
    let companyNameLabel = Heading2Label()
    let companyLogoImage = CustomImageView(image: AppStyleGuide.Images.placeholder.uiImage)

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureSubviews () {
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(companyNameLabel)
        addSubview(companyLogoImage)

        let mediumMargin = AppStyleGuide.Margins.medium.rawValue
        let smallMargin = AppStyleGuide.Margins.small.rawValue

        companyNameLabel.textColor = AppStyleGuide.Colors.white.uiColor
        companyNameLabel.textAlignment = .center

        companyLogoImage.contentMode = .scaleAspectFit

        NSLayoutConstraint.activate([
            companyLogoImage.centerXAnchor.constraint(equalTo: centerXAnchor),
            companyLogoImage.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -24),
            companyLogoImage.heightAnchor.constraint(equalToConstant: 48),
            companyLogoImage.widthAnchor.constraint(equalToConstant: 48),

            companyNameLabel.topAnchor.constraint(equalTo: companyLogoImage.bottomAnchor, constant: smallMargin),
            companyNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: mediumMargin),
            companyNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -mediumMargin),

            heightAnchor.constraint(equalToConstant: 120)
        ])
    }
}
