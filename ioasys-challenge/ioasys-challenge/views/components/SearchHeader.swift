//
//  SearchHeader.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

protocol SearchHeaderViewDelegate {
    func searchQueryDidChange (query: String) -> Void
    func onSearchFieldFocus () -> Void
    func onSearchFieldBlur () -> Void
}

class SearchHeaderView: UIView {

    var backgroundImageView = CustomImageView(image: AppStyleGuide.Images.homeHeader.uiImage)
    var heightConstraint: NSLayoutConstraint!
    var searchField = SearchTextField()
    
    let openHeight: CGFloat = 202
    let colapsedHeight: CGFloat = 92
    
    var delegate: SearchHeaderViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSubviews () {
        translatesAutoresizingMaskIntoConstraints = false

        addSubview(backgroundImageView)
        addSubview(searchField)
        
        searchField.addTarget(self, action: #selector(self.textFieldTextDidChange), for: .editingChanged)
        searchField.placeholder = "Pesquise por empresa"
        searchField.clearButtonMode = .always
        searchField.delegate = self

        let mediumMargin = AppStyleGuide.Margins.medium.rawValue

        heightConstraint = heightAnchor.constraint(equalToConstant: 188)

        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: topAnchor),
            backgroundImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundImageView.trailingAnchor.constraint(equalTo: trailingAnchor),

            searchField.topAnchor.constraint(equalTo: backgroundImageView.bottomAnchor, constant: -24),
            searchField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: mediumMargin),
            searchField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -mediumMargin),
            searchField.bottomAnchor.constraint(equalTo: bottomAnchor),
            searchField.heightAnchor.constraint(equalToConstant: 48),

            heightConstraint
        ])
    }

    func open (animated: Bool = false) {
        updateViewHeightConstraint(height: openHeight, animated: animated)
    }

    func colapse (animated: Bool = false) {
        updateViewHeightConstraint(height: colapsedHeight, animated: animated)
    }

    private func updateViewHeightConstraint (height: CGFloat, animated: Bool = false) {
        if (animated) {
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                self?.heightConstraint.constant = height
                self?.layoutIfNeeded()
            })

            return
        }

        heightConstraint.constant = height
        layoutIfNeeded()
    }
    
    @objc func textFieldTextDidChange () {
        guard let text = searchField.text else { return }
        delegate?.searchQueryDidChange(query: text)
    }
}

extension SearchHeaderView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.onSearchFieldFocus()
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.onSearchFieldBlur()
    }
}
