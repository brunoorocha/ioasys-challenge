//
//  BaseLabel.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class BaseLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultLabelConfiguration()
        customize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func defaultLabelConfiguration () {
        translatesAutoresizingMaskIntoConstraints = false
        numberOfLines = 0
        sizeToFit()
    }

    func customize() {}
}
