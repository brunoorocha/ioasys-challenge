//
//  SearchTextField.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class SearchTextField: TextField {
    override var paddingInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 48, bottom: 0, right: AppStyleGuide.Margins.medium.rawValue)
    }

    override func customize() {
        super.customize()
        font = AppStyleGuide.Typography.paragraph.uiFont

        let searchIcon = CustomImageView(image: AppStyleGuide.Icons.search.uiImage)
        addSubview(searchIcon)

        NSLayoutConstraint.activate([
            searchIcon.centerYAnchor.constraint(equalTo: centerYAnchor),
            searchIcon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: AppStyleGuide.Margins.medium.rawValue),
            searchIcon.widthAnchor.constraint(equalToConstant: 24),
            searchIcon.heightAnchor.constraint(equalToConstant: 24),
        ])
    }
}

