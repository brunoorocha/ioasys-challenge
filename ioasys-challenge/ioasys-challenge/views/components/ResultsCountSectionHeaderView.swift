//
//  ResultsCountSectionHeaderView.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class ResultsCountSectionHeaderView: UIView {
    let label = InputLabel()

    var resultsCount = 0 {
        didSet {
            if (self.resultsCount == 1) {
                label.text = "\(self.resultsCount) resultado encontrado"
                return
            }

            label.text = "\(self.resultsCount) resultados encontrados"
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureSubviews () {
        backgroundColor = AppStyleGuide.Colors.white.uiColor
        resultsCount = 0
        addSubview(label)

        let mediumMargin = AppStyleGuide.Margins.medium.rawValue
        let smallMargin = AppStyleGuide.Margins.small.rawValue

        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: mediumMargin),
            label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -mediumMargin),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -smallMargin),
            
            heightAnchor.constraint(equalToConstant: 48)
        ])
    }
}
