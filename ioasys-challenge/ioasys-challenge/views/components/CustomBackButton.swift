//
//  CustomBackButton.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 04/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class CustomBackButton: Button {
    override func customize() {
        super.customize()
        setImage(AppStyleGuide.Icons.navigateBack.uiImage, for: .normal)
        backgroundColor = AppStyleGuide.Colors.gray1.uiColor
        tintColor = AppStyleGuide.Colors.pink.uiColor
        layer.cornerRadius = 4
        clipsToBounds = true
        
        heightAnchor.constraint(equalToConstant: 40).isActive = true
        widthAnchor.constraint(equalToConstant: 40).isActive = true
    }
}
