//
//  PrimaryButton.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class PrimaryButton: Button {
    override func customize() {
        backgroundColor = AppStyleGuide.Colors.pink.uiColor
        setTitleColor(AppStyleGuide.Colors.white.uiColor, for: .normal)
        layer.cornerRadius = 8
        clipsToBounds = true
    }
}
