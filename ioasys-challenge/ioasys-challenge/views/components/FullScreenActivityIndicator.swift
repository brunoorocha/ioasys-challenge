//
//  FullScreenActivityIndicator.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class FullScreenActivityIndicator: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        customize()
        configureSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func customize () {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = UIColor.rgba(0, 0, 0, 0.5)
        layer.opacity = 0
    }

    private func configureSubviews () {
        let ioasysActivityIndicator = CustomImageView(image: AppStyleGuide.Images.loading.uiImage)
        addSubview(ioasysActivityIndicator)

        NSLayoutConstraint.activate([
            ioasysActivityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            ioasysActivityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor),
            ioasysActivityIndicator.widthAnchor.constraint(equalToConstant: 72),
            ioasysActivityIndicator.heightAnchor.constraint(equalToConstant: 72)
        ])
    }

    func show (animated: Bool = false, completion: (() -> Void)? = nil) {
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                self.layer.opacity = 1
            }, completion: { _ in
                completion?()
            })
        }

        layer.opacity = 1
        return
    }

    func hide (animated: Bool = false, completion: (() -> Void)? = nil) {
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                self.layer.opacity = 0
            }, completion: { _ in
                completion?()
            })
        }

        layer.opacity = 0
        return
    }
}
