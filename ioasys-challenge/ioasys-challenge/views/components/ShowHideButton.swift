//
//  ShowHideButton.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class ShowHideButton: Button {
    private let showIcon = AppStyleGuide.Icons.visibility.uiImage
    private let hideIcon = AppStyleGuide.Icons.noVisibility.uiImage

    var isShowing = false {
        didSet {
            if (self.isShowing) {
                setImage(hideIcon, for: .normal)
                return
            }

            setImage(showIcon, for: .normal)
        }
    }

    override func customize() {
        isShowing = false
        frame = CGRect(x: 0, y: 0, width: 24, height: 24)
    }
}
