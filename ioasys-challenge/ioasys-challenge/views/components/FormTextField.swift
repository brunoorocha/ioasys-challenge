//
//  FormTextField.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class FormTextField: UIView {
    let label = InputLabel()
    let textField = TextField()
    let showHidePasswordButton = ShowHideButton()
    let errorIconImageView = CustomImageView(image: AppStyleGuide.Icons.error.uiImage)
    let errorLabel = InputLabel()

    var isSecureTextEntry = false {
        didSet {
            showHidePasswordButton.isHidden = !self.isSecureTextEntry
            textField.isSecureTextEntry = self.isSecureTextEntry
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSubviews () {
        isSecureTextEntry = false
        errorIconImageView.isHidden = true

        addSubview(label)
        addSubview(textField)
        addSubview(showHidePasswordButton)
        addSubview(errorIconImageView)
        addSubview(errorLabel)
        
        let mediumMargin = AppStyleGuide.Margins.medium.rawValue
        let smallMargin = AppStyleGuide.Margins.small.rawValue

        showHidePasswordButton.addTarget(self, action: #selector(self.didTapOnShowHidePasswordButton), for: .touchUpInside)
        errorLabel.textColor = AppStyleGuide.Colors.red.uiColor
        errorLabel.textAlignment = .right

        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: topAnchor),
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: mediumMargin),
            label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -mediumMargin),
            
            textField.topAnchor.constraint(equalTo: label.bottomAnchor, constant: mediumMargin),
            textField.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            textField.trailingAnchor.constraint(equalTo: label.trailingAnchor),
            textField.heightAnchor.constraint(equalToConstant: 48),
            
            errorLabel.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: smallMargin),
            errorLabel.leadingAnchor.constraint(equalTo: textField.leadingAnchor),
            errorLabel.trailingAnchor.constraint(equalTo: textField.trailingAnchor),
            errorLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            showHidePasswordButton.trailingAnchor.constraint(equalTo: textField.trailingAnchor, constant: -mediumMargin),
            showHidePasswordButton.centerYAnchor.constraint(equalTo: textField.centerYAnchor),

            errorIconImageView.trailingAnchor.constraint(equalTo: textField.trailingAnchor, constant: -mediumMargin),
            errorIconImageView.centerYAnchor.constraint(equalTo: textField.centerYAnchor)
        ])
    }

    @objc func didTapOnShowHidePasswordButton () {
        showHidePasswordButton.isShowing.toggle()
        textField.isSecureTextEntry = !showHidePasswordButton.isShowing
    }
    
    func hasErrors (errorMessage: String) {
        textField.layer.borderColor = AppStyleGuide.Colors.red.uiColor.cgColor
        errorIconImageView.isHidden = false
        showHidePasswordButton.isHidden = true
        errorLabel.text = errorMessage
    }

    func clearErrors () {
        textField.layer.borderColor = AppStyleGuide.Colors.gray1.uiColor.cgColor
        errorIconImageView.isHidden = true
        showHidePasswordButton.isHidden = !isSecureTextEntry
        errorLabel.text = ""
    }
}
