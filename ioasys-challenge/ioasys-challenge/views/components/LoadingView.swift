//
//  LoadingView.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    let loading = CustomImageView(image: AppStyleGuide.Images.loading.uiImage)

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureSubviews () {
        backgroundColor = AppStyleGuide.Colors.white.uiColor
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(loading)

        NSLayoutConstraint.activate([
            loading.centerXAnchor.constraint(equalTo: centerXAnchor),
            loading.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
}
