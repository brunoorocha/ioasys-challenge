//
//  FormSubmitButton.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class FormSubmitButton: UIView {
    let button = PrimaryButton()
    
    var title = "" {
        didSet {
            button.setTitle(self.title, for: .normal)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSubviews () {
        addSubview(button)
        let mediumMargin = AppStyleGuide.Margins.medium.rawValue

        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: topAnchor),
            button.leadingAnchor.constraint(equalTo: leadingAnchor, constant: mediumMargin),
            button.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -mediumMargin),
            button.bottomAnchor.constraint(equalTo: bottomAnchor),
            button.heightAnchor.constraint(equalToConstant: 48)
        ])
    }
}

