//
//  ParagraphLabel.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class ParagraphLabel: BaseLabel {
    override func customize() {
        font = AppStyleGuide.Typography.paragraph.uiFont
        textColor = AppStyleGuide.Colors.black.uiColor
    }
}

