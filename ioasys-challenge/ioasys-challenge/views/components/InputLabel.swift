//
//  InputLabel.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

class InputLabel: BaseLabel {
    override func customize() {
        font = AppStyleGuide.Typography.label.uiFont
        textColor = AppStyleGuide.Colors.gray3.uiColor
    }
}

