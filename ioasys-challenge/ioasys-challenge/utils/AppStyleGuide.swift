//
//  AppStyleGuide.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 02/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import UIKit

enum AppStyleGuide {

    // MARK: Margins
    enum Margins: CGFloat {
        case xsmall = 4.0
        case small = 8.0
        case medium = 16.0
        case large = 32.0
        case xlarge = 64.0
    }

    // MARK: Typography
    enum Typography {
        case heading1
        case heading2
        case heading3
        case paragraph
        case label
        case textInput
        case button

        var uiFont: UIFont {
            switch self {
            case .heading1:
                return UIFont(name: "Rubik-Medium", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
            case .heading2:
                return UIFont(name: "Rubik-Black", size: 18.0) ?? UIFont.systemFont(ofSize: 18.0)
            case .heading3:
                return UIFont(name: "Rubik-Regular", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
            case .paragraph:
                return UIFont(name: "Rubik-Light", size: 18.0) ?? UIFont.systemFont(ofSize: 18.0)
            case .label:
                return UIFont(name: "Rubik-Light", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
            case .textInput:
                return UIFont(name: "Rubik-Light", size: 16.0) ?? UIFont.systemFont(ofSize: 16.0)
            case .button:
                return UIFont(name: "Rubik-Medium", size: 16.0) ?? UIFont.systemFont(ofSize: 16.0)
            }
        }
    }

    // MARK: Colors
    enum Colors {
        case pink
        case pink2
        case gray1
        case gray2
        case gray3
        case gray4
        case white
        case black
        case red
        case blue
        case solmon
        case green

        var uiColor: UIColor {
            switch self {
            case .pink:
                return UIColor.rgba(224, 30, 105)
            case .pink2:
                return UIColor.rgba(251, 219, 231)
            case .gray1:
                return UIColor.rgba(245, 245, 245)
            case .gray2:
                return UIColor.rgba(204, 204, 204)
            case .gray3:
                return UIColor.rgba(102, 102, 102)
            case .gray4:
                return UIColor.rgba(60, 60, 60)
            case .white:
                return UIColor.rgba(255, 255, 255)
            case .black:
                return UIColor.rgba(0, 0, 0)
            case .red:
                return UIColor.rgba(235, 87, 87)
            case .blue:
                return UIColor.rgba(121, 187, 202)
            case .solmon:
                return UIColor.rgba(235, 151, 151)
            case .green:
                return UIColor.rgba(144, 187, 129)
            }
        }
    }

    // MARK: Icons
    enum Icons {
        case error
        case visibility
        case noVisibility
        case search
        case navigateBack

        var uiImage: UIImage? {
            switch self {
            case .error:
                return UIImage(named: "error-icon")
            case .visibility:
                return UIImage(named: "visibility-icon")
            case .noVisibility:
                return UIImage(named: "no-visibility-icon")
            case .search:
                return UIImage(named: "search-icon")
            case .navigateBack:
                return UIImage(named: "left-action")
            }
        }
    }

    // MARK: Images
    enum Images {
        case loading
        case ioasysLogo
        case loginHeader
        case homeHeader
        case placeholder

        var uiImage: UIImage? {
            switch self {
            case .loading:
                return UIImage(named: "loading")
            case .ioasysLogo:
                return UIImage(named: "logo-home")
            case .loginHeader:
                return UIImage(named: "login-header")
            case .homeHeader:
                return UIImage(named: "home-header")
            case .placeholder:
                return UIImage(named: "image-placeholder")
            }
        }
    }
}
