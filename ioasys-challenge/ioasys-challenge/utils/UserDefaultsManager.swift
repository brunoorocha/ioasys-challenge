//
//  UserDefaultsManager.swift
//  ioasys-challenge
//
//  Created by Bruno Rocha on 03/04/20.
//  Copyright © 2020 Bruno Rocha. All rights reserved.
//

import Foundation

struct UserDefaultsManager {
    static var shared = UserDefaultsManager()
    private init () {}
    
    enum Keys: String {
        case accessToken = "access_token"
        case client = "client"
        case uid = "uid"
    }

    var accessToken: String? {
        return UserDefaults.standard.string(forKey: Keys.accessToken.rawValue)
    }
    
    var client: String? {
        return UserDefaults.standard.string(forKey: Keys.client.rawValue)
    }

    var uid: String? {
        return UserDefaults.standard.string(forKey: Keys.uid.rawValue)
    }

    func setAccessToken (token: String) {
        UserDefaults.standard.setValue(token, forKey: Keys.accessToken.rawValue)
    }

    func setClient (token: String) {
        UserDefaults.standard.setValue(token, forKey: Keys.client.rawValue)
    }

    func setUID (token: String) {
        UserDefaults.standard.setValue(token, forKey: Keys.uid.rawValue)
    }
}
